// import axios from "axios";
import Vue from "vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

// bootstrap
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";


// Install BootstrapVue
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);

export default {
    name: "Login",
    data() {
        return {
            username: "",
            password: "",
            submitted: false,
            loading: false,
            returnUrl: "",
            error: ""
        };
    },
    // methods: {
    //     login() {
    //         this.submitted = true;
    //         const { username, password } = this;
    //         // console.log(username, password);
    //         if (!(username && password)) {
    //             return false;
    //         }
    //         this.loading = true;
    //         return axios({
    //             url: "v1/auth",
    //             method: "GET",
    //             headers: {
    //                 "Content-Type": "application/json",
    //                 "Access-Control-Allow-Origin": "*",
    //                 Authorization: "Basic " + window.btoa(username + ":" + password)
    //             }
    //         })
    //             .then(res => {
    //                 if (res.status === 200) {
    //                     localStorage.setItem("token", res.data.token);
    //                     localStorage.setItem(
    //                         "user_access",
    //                         JSON.stringify(res.data.user_access)
    //                     );
    //                     console.log(res.data.token);
    //                     this.error = false;
    //                     window.location = "/admin/dashboard";
    //                 }
    //             })
    //             .catch(err => {
    //                 let messageError = JSON.parse(err.request.response);
    //                 this.error = messageError.message;
    //                 this.loading = false;
    //                 console.log(messageError.message);
    //             });
    //     }
    // }
};
