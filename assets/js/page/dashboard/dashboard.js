$(document).ready(function() {

    // CAROUSEL
    
    $('.carousel[data-type="multi"] .item').each(function () {
        var next = $(this).next();
    
        if ($(window).width() > 768) {
            if (!next.length) {
                next = $(this).siblings(':first');
            }
    
            next.children(':first-child').clone().appendTo($(this));
    
            for (var i = 0; i < 1; i++) {
                next = next.next();
                if (!next.length) {
                    next = $(this).siblings(':first');
                }
    
                next.children(':first-child').clone().appendTo($(this));
            }
        }
    });
    
    var section_a_height = $("#section-a-parent").height(),
        section_b_height = $("#section-b-parent").height();

    $(".section-a-child").css({'height': (section_a_height - 34) + 'px'});
    $(".section-b-child").css({'height': (section_b_height - 34) + 'px'});

    $("input[type=radio][name=usage-number]").on('change', function() {
        usage_config.done == true;
        
        if ($(this).val() == 'production') {
            usage_config.next = 'by-project-production';
        } else if ($(this).val() == 'development') {
            usage_config.next = 'by-project-development';
        }

        execute_usage();
    });

    // PIE CHART

    var isBig = $(window).width() > 700;

    var legendBig = {
        align: 'right',
        verticalAlign: 'middle',
        layout: 'vertical'
    };

    var legendSmall = {
        align: 'center',
        verticalAlign: 'bottom',
        layout: 'horizontal'
    }

    // MESSAGE USAGE DISTRIBUTION

    var TopClientChart = new Highcharts.chart('top-client', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            spacingBottom: 40
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        legend: isBig ? legendBig : legendSmall,
        tooltip: {
            headerFormat: '<b>{point.key}</b><br/>',
            pointFormat: '{point.y:.0f} ({point.percentage:.1f} %)',
            valueDecimals: 2
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: "{point.percentage:.0f}%",
                    distance: -30,
                    color: "white",
                    style: {
                        textOutline: 0,
                        fontWeight: 'normal'
                    }
                },
                showInLegend: true
            }
        },
        series: [{
            name: "Message",
            colorByPoint: true,
            data: [],
        }]
    });

    var top_client = {
        'load' : function() {
            $.ajax({
                url: "/admin/dashboard/dashboard/top-client",
                type: "GET",
                async: true,
                contentType: "JSON"
            }).done(function (data) {
                var parse_data = JSON.parse(data);

                TopClientChart.series[0].setData(parse_data, true, false);

                $('#top-client').parent().find('.card-loader').removeClass('display-block').addClass('display-none');
            }).fail(function(data, textStatus, xhr) {
                // top_client.load();
                console.log(textStatus);
            });
        }
    }
    
    top_client.load();

    // TOP REQUEST TYPE

    var TopRequestTypeChart = new Highcharts.chart('top-request-type', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            spacingBottom: 40
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        legend: isBig ? legendBig : legendSmall,
        tooltip: {
            headerFormat: '<b>{point.key}</b><br/>',
            pointFormat: '{point.y:.0f} ({point.percentage:.1f} %)',
            valueDecimals: 2
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: "{point.percentage:.0f}%",
                    distance: -30,
                    color: "white",
                    style: {
                        textOutline: 0,
                        fontWeight: 'normal'
                    }
                },
                showInLegend: true
            }
        },
        series: [{
            name: "Message",
            colorByPoint: true,
            data: [],
        }]
    });

    var top_request_type = {
        'load' : function() {
            $.ajax({
                url: "/admin/dashboard/dashboard/top-request-type",
                type: "GET",
                async: true,
                contentType: "JSON"
            }).done(function (data) {
                var parse_data = JSON.parse(data);

                TopRequestTypeChart.series[0].setData(parse_data, true, false);

                $('#top-request-type').parent().find('.card-loader').removeClass('display-block').addClass('display-none');
            }).fail(function(data, textStatus, xhr) {
                // top_request_type.load();
                console.log(textStatus);
            });
        }
    }
    
    top_request_type.load();

    // STATISTIC

    var statistic_type = 'line';

    // STATISTIC - LOG API

    var ApiLogStatisticChart = new Highcharts.chart('api-log', {
        chart: {
            type: statistic_type
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        yAxis: {
            title: false
        },
        series: [{
            name: 'success',
            color: '#31D65D',
            data: [],
            marker : {symbol : 'circle'}
        }, {
            name: 'error',
            color: '#EF3F51',
            data: [],
            marker : {symbol : 'circle'}
        }],
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 1000
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'left',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    });

    var api_log_config = {
        'done' : true,
        'next' : null,
        'load' : function(type, async, element) {
            if (this.done) {
                this.done = false;

                $.ajax({
                    url: "/admin/dashboard/dashboard/api-log/" + type,
                    type: "GET",
                    async: async,
                    contentType: "JSON"
                }).done(function (data) {
                    switch (type) {
                        case 'chart':
                            var summary = JSON.parse(data);
                            // console.log(summary);
                            var api_log_date = [],
                                capi_log_success = [],
                                api_log_error = [];

                            api_log_date       = summary.date;
                            api_log_success    = summary.success.map(function(x) { return parseInt(x) });
                            api_log_error      = summary.error.map(function(x) { return parseInt(x) });

                            ApiLogStatisticChart.xAxis[0].setCategories(eval(api_log_date), false, true);
                            ApiLogStatisticChart.series[0].setData(api_log_success, true, true);
                            ApiLogStatisticChart.series[1].setData(api_log_error, true, true);

                            element.find('.card-loader').removeClass('display-block').addClass('display-none');
                            api_log_config.next = 'scheduled';
                            api_log_config.done = true;
                            execute_api_log();
                            break;
                        default:
                            break;
                    }
                }).fail(function(data, textStatus, xhr) {
                    // api_log_config.done = true;
                    // execute_api_log();
                    console.log(textStatus);
                });
            }
        }
    }

    var execute_api_log = function() {
        if (api_log_config.done == true && api_log_config.next == null) {
            let api_log_chart = $(".campaign-chart");

            if (!api_log_chart.find('.card-loader').hasClass('display-block')) {
                api_log_chart.find('.card-loader').addClass('display-block');
            }

            api_log_config.load('chart', true, api_log_chart);
        }

        if (api_log_config.done == true && api_log_config.next == 'scheduled') {
            let api_log_scheduled = $(".campaign-scheduled");

            if (!api_log_scheduled.find('.card-loader').hasClass('display-block')) {
                api_log_scheduled.find('.card-loader').addClass('display-block');
            }

            api_log_config.load('scheduled', true, api_log_scheduled);
        }

        if (api_log_config.done == true && api_log_config.next == 'automation') {
            let api_log_automation = $(".campaign-automation");

            if (!api_log_automation.find('.card-loader').hasClass('display-block')) {
                api_log_automation.find('.card-loader').addClass('display-block');
            }

            api_log_config.load('automation', true, api_log_automation);
        }
    }

    execute_api_log();

    // execute_bounce_and_sms();

    var switch_line = $('#switch-line'),
        switch_column = $('#switch-column');

    switch_line.on('click', function() {
        switch_column.removeClass('active');
        $(this).addClass('active');
        
        for(var a = 0; a < ApiLogStatisticChart.series.length; a++) {
            ApiLogStatisticChart.series[a].update({
                type: 'line'
            });
        }
    })

    switch_column.on('click', function() {
        switch_line.removeClass('active');
        $(this).addClass('active');

        for(var a = 0; a < ApiLogStatisticChart.series.length; a++) {
            ApiLogStatisticChart.series[a].update({
                type: 'column'
            });
        }
    })

    var bulan = $('#bulan');
    var value = $('#bulan').val();

    bulan.change(function() {
        bln = $('#bulan').val();
        type = 'chart';
                $.ajax({
                    url: "/admin/dashboard/dashboard/api-log/chart",
                    type: "GET",  
                    data: {bln: bln},
                    contentType: "JSON"
                }).done(function (data) {
                    switch (type) {
                        case 'chart':
                            var summary = JSON.parse(data);
                            var api_log_date = [],
                                capi_log_success = [],
                                api_log_error = [];

                            api_log_date       = summary.date;
                            api_log_success    = summary.success.map(function(x) { return parseInt(x) });
                            api_log_error      = summary.error.map(function(x) { return parseInt(x) });

                            ApiLogStatisticChart.xAxis[0].setCategories(eval(api_log_date), false, true);
                            ApiLogStatisticChart.series[0].setData(api_log_success, true, true);
                            ApiLogStatisticChart.series[1].setData(api_log_error, true, true);

                            // element.find('.card-loader').removeClass('display-block').addClass('display-none');
                            api_log_config.next = 'scheduled';
                            api_log_config.done = true;
                            execute_api_log();
                            break;
                        default:
                            break;
                    }
                }).fail(function(data, textStatus, xhr) {
                    // api_log_config.done = true;
                    // execute_api_log();
                    console.log(textStatus);
                });
    })
});