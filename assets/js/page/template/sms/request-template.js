var editor = $('.editor-variable');
var evBold = $('.ev_bold');
var evItalic = $('.ev_italic');
var evVariable = $('.ev_variable');
var evStrike = $('.ev_strike');
var evCode = $('.ev_code');
var counter = 1;
var objectSelection = '',
    beforeSelection = '',
    afterSelection = '',
    selStart, selEnd;

editor.on('keyup mouseup', function() {
    getSelectionText();
    getVariableCount();
    $('#ttl').val(getVariableCount());
    // reCount();
    $('.ev_param').val(getVariableCount());
});

function replaceRange(s, start, end, substitute) {
    return s.substring(0, start) + substitute + s.substring(end);
}

function reOrderVar(string){
    var regex = /{{\d+}}/g;

    var arr = [], newString = '', stringMasking = '', counter = 1, endLength=5;
    // masking variable {{-number-}} with {{-character-}}
    stringMasking = string.replace(regex, '{{;}}');

    // get the length of the mask character
    // replace the mask with new value
    stringMasking.replace(/{{;}}/g, function(match, strLength) {
        var varCounter = '{{'+counter+'}}';
        if(newString !== ''){
            var newStartLength = newString.indexOf('{{;}}');
            newString = replaceRange(newString, newStartLength, newStartLength+endLength, varCounter);
        } else {
            newString = replaceRange(stringMasking, strLength, strLength+endLength, varCounter);
        }

        counter++;
        return newString;
    });

    return newString;
}

evVariable.click(function() {
    getSelectionText();
    getVariableCount();

    var finalString = beforeSelection + afterSelection + '{{' + (getVariableCount() + 11) + '}} ';

    var newString = reOrderVar(finalString);

    editor.val(newString);
    editor.focus();

    $('#ttl').val(getVariableCount());
    $('.ev_param').val(getVariableCount());
});

evBold.click(function() {
    isFormatted('*');
});

evItalic.click(function() {
    isFormatted('_');
});

evStrike.click(function() {
    isFormatted('~');
});

evCode.click(function() {
    isFormatted('```', 3);
});

function setContent(type, step = 1)
{
    getSelectionText();
    var finalString = beforeSelection + type + objectSelection + type + afterSelection;
    editor.val(finalString);
    editor.focus();
    document.getElementsByClassName('editor-variable')[0].setSelectionRange(selStart + step, selEnd + step);
}

function isFormatted(format, step = 1)
{
    getSelectionText();
    var original = editor.val();

    var prefix = original.substr((selStart - step), step);
    var suffix = original.substr(selEnd, step);

    if (prefix == format && suffix == format) {
        var pref_string = original.substr(0, (selStart - step));
        var suff_string = original.substr((selEnd + step), original.length);

        editor.val(pref_string + objectSelection + suff_string);
        editor.focus();
        document.getElementsByClassName('editor-variable')[0].setSelectionRange((selStart - step), (selEnd - step));
    } else {
        setContent(format, step);
    }
}

function getSelectionText()
{
    selStart = editor[0].selectionStart;
    selEnd = editor[0].selectionEnd;

    var originalString = editor.val();

    var segment_1 = originalString.substr(0, selStart);
    var segment_2 = originalString.substr(selStart, (selEnd - selStart));
    var segment_3 = originalString.substr(selEnd, originalString.length);

    beforeSelection = segment_1;
    objectSelection = segment_2;
    afterSelection = segment_3;

    reCount();
}

function getVariableCount()
{
    var regex = /{{\d+}}/g,
        val = editor.val(),
        match = val.match(regex),
        result = (match) ? match.length : 0;

    return result;
}

function reCount(){
    jum = parseInt($('#ttl').val());

    dreal = parseInt(0);
    var arrList=[0];
    //console.log('jum'+jum);
    if(jum>0){
        for(i=1;i<=jum;i++){

            regx="{{["+i+"]*}}";
            re = new RegExp(regx);
            val = editor.val();
            match = re.test(val);

            if(match){
                dreal++;
            }

        }

        $('.ev_param').val(dreal);
    }
    else{
        $('.ev_param').val(0);
        //return 0;
    }
    // return dreal;
}

! function (a) {
    "use strict";
    a.fn.extend({
        counter: function (b) {
            var c = {
                    type: "char",
                    count: "down",
                    goal: 140,
                    text: !0,
                    target: !1,
                    append: !0,
                    translation: "",
                    msg: "",
                    containerClass: ""
                },
                d = "",
                e = "",
                f = !1,
                b = a.extend({}, c, b),
                g = {
                    init: function (c) {
                        var e = c.attr("id"),
                            f = e + "_count";
                        g.isLimitless(), d = a("<span id=" + f + "/>");
                        var h = a("<div role='status'/>").attr("id", e + "_counter").append(d).append(" " + g.setMsg());
                        b.containerClass && b.containerClass.length && h.addClass(b.containerClass), b.target && a(b.target).length ? b.append ? a(b.target).append(h) : a(b.target).prepend(h) : b.append ? h.insertAfter(c) : h.insertBefore(c), c.attr("aria-controls", e + "_counter"), g.bind(c)
                    },
                    bind: function (a) {
                        a.on("keypress.counter keydown.counter keyup.counter blur.counter focus.counter change.counter paste.counter", g.updateCounter), a.on("keydown.counter", g.doStopTyping), a.trigger("keydown")
                    },
                    isLimitless: function () {
                        return "sky" === b.goal ? (b.count = "up", f = !0) : void 0
                    },
                    setMsg: function () {
                        if ("" !== b.msg) return b.msg;
                        if (b.text === !1) return "";
                        if (f) return "" !== b.msg ? b.msg : "";
                        switch (this.text = b.translation || "character word left max", this.text = this.text.split(" "), this.chars = "s ( )".split(" "), this.msg = null, b.type) {
                            case "char":
                                b.count === c.count && b.text ? this.msg = this.text[0] + this.chars[1] + this.chars[0] + this.chars[2] + " " + this.text[2] : "up" === b.count && b.text && (this.msg = this.text[0] + this.chars[0] + " " + this.chars[1] + b.goal + " " + this.text[3] + this.chars[2]);
                                break;
                            case "word":
                                b.count === c.count && b.text ? this.msg = this.text[1] + this.chars[1] + this.chars[0] + this.chars[2] + " " + this.text[2] : "up" === b.count && b.text && (this.msg = this.text[1] + this.chars[1] + this.chars[0] + this.chars[2] + " " + this.chars[1] + b.goal + " " + this.text[3] + this.chars[2])
                        }
                        return this.msg
                    },
                    getWords: function (b) {
                        return "" !== b ? a.trim(b).replace(/\s+/g, " ").split(" ").length : 0
                    },
                    updateCounter: function (f) {
                        var h = "true" == a(this).attr("contentEditable") ? a(this).text() : a(this).val();
                        (0 > e || e > b.goal) && g.passedGoal(a(this)), b.type === c.type ? b.count === c.count ? (e = b.goal - h.length, 0 >= e ? d.text("0") : d.text(e)) : "up" === b.count && (e = h.length, d.text(e)) : "word" === b.type && (b.count === c.count ? (e = g.getWords(h), e <= b.goal ? (e = b.goal - e, d.text(e)) : d.text("0")) : "up" === b.count && (e = g.getWords(h), d.text(e)))
                    },
                    doStopTyping: function (a) {
                        var d = [46, 8, 9, 35, 36, 37, 38, 39, 40, 32];
                        return g.isGoalReached(a) && a.keyCode !== d[0] && a.keyCode !== d[1] && a.keyCode !== d[2] && a.keyCode !== d[3] && a.keyCode !== d[4] && a.keyCode !== d[5] && a.keyCode !== d[6] && a.keyCode !== d[7] && a.keyCode !== d[8] ? b.type === c.type ? !1 : a.keyCode !== d[9] && a.keyCode !== d[1] && b.type != c.type : void 0
                    },
                    isGoalReached: function (a, d) {
                        return f ? !1 : b.count === c.count ? (d = 0, d >= e) : (d = b.goal, e >= d)
                    },
                    wordStrip: function (b, c) {
                        var d = c.replace(/\s+/g, " ").split(" ").length;
                        return c = a.trim(c), 0 >= b || b === d ? c : (c = a.trim(c).split(" "), c.splice(b, d, ""), a.trim(c.join(" ")))
                    },
                    passedGoal: function (a) {
                        var c = a.val();
                        "word" === b.type && a.val(g.wordStrip(b.goal, c)), "char" === b.type && a.val(c.substring(0, b.goal)), "down" === b.type && d.val("0"), "up" === b.type && d.val(b.goal)
                    }
                };
            return this.each(function () {
                g.init(a(this))
            })
        }
    })
}(jQuery);

$("textarea").counter({
    count: 'down',
    goal: 160,
    target: '.counter span',
    text: false
});