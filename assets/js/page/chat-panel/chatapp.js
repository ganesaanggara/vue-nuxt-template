Vue.directive('emoji', {
    inserted (el) {
        vm.testEmoji = el.innerHTML;

        function unescapeUnicode(str) {
            return str.replace(/\\u([a-fA-F0-9]{4})/g, function(g, m1) {
                return String.fromCharCode(parseInt(m1, 16));
            });
        }
        el.innerHTML = twemoji.parse(unescapeUnicode(vm.testEmoji));
    }
});

Vue.directive('fetch', {
    bind: function (el, binding, vnode) {
        var dataSource = binding.value;
        var message = dataSource.message;
        var index = dataSource.index;

        if (message.wa_media_id != null && message.file_name == null && message.download_status == 0) {
            // console.log(message)
            const host = vm.getHost();
            axios.post(host + '/client/chat-panel/download-media', {
                mediaId: message.wa_media_id,
                chatId: message.id,
                index: index
            })
            .then(function(response) {
                var responseData = response.data;
                var messageIndex = responseData.data.messageIndex;
                var messageFileName = responseData.data.fileName;

                vm.messageList[messageIndex].file_name = messageFileName;
                vm.messageList[messageIndex].download_status = 2;
            })
            .catch(function(err) {
                console.log('Error: ' + err);
            });
        }
    }
});

Vue.component('editable', {
    template: `<div class="send-text-input" @keyup="press" contenteditable="true" v-once v-html="value" :value="value" @input="$emit('input', $event.target.innerHTML)"></div>`,
    props: ['value', 'press'],
    watch: {
        value: function (newValue) {
            if (document.activeElement == this.$el) {
                return;
            }

            this.$el.innerHTML = newValue;
        }
    }
});

Vue.component('content-caption', {
    template: `<div class="send-caption-input" @keydown="limit" @keyup="press" data-maxlength="5" autocofus contenteditable="true" v-once v-html="value" :value="value" @input="$emit('input', $event.target.innerHTML)"></div>`,
    props: ['value', 'press', 'limit'],
    watch: {
        value: function (newValue) {
            if (document.activeElement == this.$el) {
                return;
            }

            this.$el.innerHTML = newValue;
        }
    }
});

var vm = new Vue({
    el: '#chatPanelApp',
    data: {
        placeholders: true,
        chatResults: [],
        messageList: [],
        noChat: true,
        isLoading: false,
        isRequest: false,
        isRejected: false,
        isActive: false,
        message: '',
        chatCode: '',
        searchChat: '',
        totalChatRequest: 0,
        totalChatActive: 0,
        isTotalActive: true,
        isTotalRequest: true,
        isPanelHistory: false,
        filteredChat: [],
        chatRequest: {
            number: null,
            projectName: null,
            requestedOn: null,
            sessionId: null,
            chatId: null,
            index: 0,
            name: null,
        },
        currentDate: Date(),
        scrollHeight: 0,
        currentChat: {
            chatCode: '',
            agentId: '',
            clientId: '',
            chatId: '',
            name: '',
            number: '',
            projectId: '',
            projectName: ''
        },
        activeMenu: null,
        lastMessage: null,
        watcher: null,
        scrollButton: false,
        chatWatcher: null,
        isExpanded: false,
        tokenSource: null,
        emojiExpanded: false,
        emojiActive: 0,
        emojiClick: false,
        emojiGroupExpanded: false,
        messageContent: '',
        file: '',
        urlPreview: null,
        caption: ''
    },
    methods: {
        getHost: function() {
            var protocol = location.protocol;
            var slashes = protocol.concat('//');

            return slashes.concat(window.location.hostname);
        },
        fetchChat: function() {
            var host = this.getHost();
            vm = this;
            clearInterval(vm.watcher);
            const cancelToken = axios.CancelToken;
            vm.tokenSource = cancelToken.source();
            axios.get(host + '/client/chat-panel/get-chat', {
                cancelToken: vm.tokenSource.token
            })
                .then(function(response) {
                    const responseData = response.data;
                    if (responseData.status == 200) {
                        vm.placeholders = false;
                        vm.chatResults = responseData.data;
                        vm.totalChatRequest = vm.chatResults.filter(function(chat) {
                            return chat.agent_id == null;
                        }).length;
                        vm.totalChatActive = vm.chatResults.filter(function(chat) {
                            return chat.active == 1 && chat.agent_id != null;
                        }).length;
                        vm.isLoading = false;
                        vm.isRequest = false;
                        vm.isActive = false;
                        vm.requestChatWatcher();
                        vm.filterChat();
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        filterChat: function() {
            vm = this;
            vm.filteredChat = vm.chatResults.filter((chat) => {
                if (!vm.isTotalRequest && vm.isTotalActive) {
                    return (chat.number.includes(vm.searchChat) || chat.name.toLowerCase().includes(vm.searchChat.toLowerCase())) && chat.agent_id != null;
                } else if (!vm.isTotalRequest && !vm.isTotalActive) {
                    return (chat.number.includes(vm.searchChat) || chat.name.toLowerCase().includes(vm.searchChat.toLowerCase())) &&
                        chat.agent_id != null &&
                        chat.active == '0';
                } else if (vm.isTotalRequest && !vm.isTotalActive) {
                    return (chat.number.includes(vm.searchChat) || chat.name.toLowerCase().includes(vm.searchChat.toLowerCase())) &&
                        chat.agent_id == null &&
                        chat.active == '1';
                }

                return chat.number.includes(vm.searchChat) || chat.name.toLowerCase().includes(vm.searchChat.toLowerCase());
            })
        },
        timeAgo: function (current, previous) {
            var msPerMinute = 60 * 1000;
            var msPerHour = msPerMinute * 60;
            var msPerDay = msPerHour * 24;
            var msPerMonth = msPerDay * 30;
            var msPerYear = msPerDay * 365;

            var elapsed = current - previous;

            if (elapsed < msPerMinute) {
                return Math.round(elapsed/1000) + ' seconds ago';
            }

            else if (elapsed < msPerHour) {
                return Math.round(elapsed/msPerMinute) + ' minutes ago';
            }

            else if (elapsed < msPerDay ) {
                return Math.round(elapsed/msPerHour ) + ' hours ago';
            }

            else if (elapsed < msPerMonth) {
                return Math.round(elapsed/msPerDay) + ' days ago';
            }

            else if (elapsed < msPerYear) {
                return Math.round(elapsed/msPerMonth) + ' months ago';
            }

            else {
                return Math.round(elapsed/msPerYear ) + ' years ago';
            }
        },
        getChatMessage: function(number, chatCode, index) {
            if (vm.chatCode == chatCode) {
                return false;
            }
            var host = this.getHost();
            vm = this;
            vm.isLoading = true;
            vm.activeMenu = chatCode;
            vm.isActive = false;
            vm.chatCode = chatCode;
            clearInterval(vm.watcher);

            // check chat status (request | exist)
            axios.get(host + '/client/chat-panel/check-chat', {params: {number: number}})
                .then(function(response) {
                    // if chat is new request
                    if (response.data != 0) {
                        axios.get(host + '/client/chat-panel/get-single-chat', {params: {id: response.data}})
                            .then(function(response) {
                                vm.isLoading = false;
                                vm.noChat = false;
                                if (typeof response.data === 'object') {
                                    var data = response.data;
                                    vm.chatRequest.number = data.number;
                                    vm.chatRequest.projectName = data.project_name;
                                    vm.chatRequest.requestedOn = data.created_date;
                                    vm.chatRequest.sessionId = data.session_id;
                                    vm.chatRequest.chatId = data.chat_code;
                                    vm.chatRequest.name = data.name;
                                    vm.chatRequest.index = index;
                                    vm.isRequest = true;
                                }
                            });
                    // if chat not request
                    } else {
                        if (vm.isPanelHistory) {
                            isHistory = '1';
                        } else {
                            isHistory = '0';
                        }

                        axios.post(host + '/client/chat-panel/get-message', {
                            number: number,
                            chatCode: chatCode,
                            isHistory: isHistory,
                        })
                            .then(function(response) {
                                var responseData = response.data;
                                vm.currentChat.projectName = responseData.data.projectName;
                                vm.currentChat.chatCode = chatCode;
                                vm.currentChat.name = responseData.data.name;
                                vm.currentChat.number = number;
                                vm.currentChat.agentName = responseData.data.agentName;
                                vm.currentChat.startDate = responseData.data.startDate;
                                vm.currentChat.endDate = responseData.data.endDate;
                                vm.currentChat.sessionId = responseData.data.sessionId;
                                vm.currentChat.duration = responseData.data.duration;

                                if (responseData.data.messages.length > 0) {
                                    if (responseData.data.messages.length > 0) {
                                        vm.lastMessage = responseData.data.messages[responseData.data.messages.length - 1].created_date;
                                    } else {
                                        vm.lastMessage = null;
                                    }

                                    if (responseData.data.isActive == 0) {
                                        vm.isActive = false;
                                    } else {
                                        vm.isActive = true;
                                        // vm.chatCode = responseData.data.messages[responseData.data.messages.length - 1].chat_code;
                                        vm.chatCode = chatCode;
                                        vm.watcher = null;
                                        // console.log('watcher starting again');
                                        vm.watcher = setInterval(function() {
                                            vm.updateMessage();
                                        }, 3000);
                                    }
                                    vm.messageList = responseData.data.messages;
                                } else {
                                    if (responseData.data.isActive == 1) {
                                        vm.isActive = true;
                                    } else {
                                        vm.isActive = false;
                                    }

                                    vm.messageList = [];
                                }

                                vm.isLoading = false;
                                vm.noChat = false;
                                vm.isRequest = false;
                                // alert('oke')
                                vm.scrollToBottom();
                                vm.buttonScroll();
                            }).catch(function(error) {
                                console.log(error);
                            });
                    }
                }).catch(function (error) {
                    console.log(error);
                });
        },
        convertDate: function(date) {
            var t = date.split(/[- :]/);
            // return new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
            return new Date(date);
        },
        rejectChat: function(chatCode) {
            vm.isLoading = true;
            var host = this.getHost();
            clearInterval(vm.watcher);
            axios.post(host + '/client/chat-panel/reject', {
                chatCode: chatCode
            })
                .then(function (response) {
                    vm.placeholders = true;
                    vm.fetchChat();
                    vm.noChat = true;
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        acceptChat: function(chatCode) {
            var host = this.getHost();
            vm = this;
            vm.isLoading = true;
            vm.noChat = false;
            vm.isRequest = false;
            clearInterval(vm.watcher);
            axios.post(host + '/client/chat-panel/accept', {
                chatCode: chatCode
            })
                .then(function (response) {
                    var responseData = response.data;

                    if (responseData.status == 200) {
                        vm.chatCode = chatCode;
                        vm.getActiveChat(responseData.data.chatCode);
                        vm.currentChat.chatCode = responseData.data.chatCode;
                        vm.currentChat.agentId = responseData.data.agent_id;
                        vm.currentChat.chatId = responseData.data.chatId;
                        vm.currentChat.clientId = responseData.data.clientId;
                        vm.currentChat.name = responseData.data.name;
                        vm.currentChat.number = responseData.data.number;
                        vm.currentChat.projectId = responseData.data.projectId;
                        vm.currentChat.projectName = responseData.data.projectName;
                        vm.chatResults[vm.chatRequest.index].agent_id = responseData.data.agent_id;
                        vm.totalChatRequest -= 1;
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        getActiveChat: function(chatCode) {
            var host = this.getHost();
            vm = this;
            clearInterval(vm.watcher);
            axios.get(host + '/client/chat-panel/active-chat', {params: { chatCode: chatCode }})
                .then(function(response) {
                    var responseData = response.data;
                    vm.currentChat.chatCode = chatCode;
                    vm.currentChat.name = responseData.data.detail.name;

                    if (responseData.status == 204) {
                        vm.currentChat.projectName = responseData.data.detail.project_name;
                        vm.lastMessage = null;
                    } else if (responseData.status == 200) {
                        vm.currentChat.projectName = responseData.data.detail.project_name;
                        if (responseData.data.messages.length > 0) {
                            vm.lastMessage = responseData.data.messages[responseData.data.messages.length - 1].created_date;
                        } else {
                            vm.lastMessage = null;
                        }
                    }
                    vm.currentChat.number = responseData.data.detail.number;

                    // if (responseData.data.messages.length > 0) {
                    //     vm.lastMessage = responseData.data.messages[responseData.data.messages.length - 1].created_date;
                    // } else {
                    //     vm.lastMessage = null;
                    // }

                    if (responseData.status == 200) {
                        vm.messageList = responseData.data.messages;
                    } else {
                        vm.messageList = [];
                    }

                    vm.filteredChat = vm.sortingChat(vm.chatResults);
                    vm.activeMenu = chatCode;

                    vm.watcher = setInterval(function() {
                        vm.updateMessage();
                    }, 3000);

                    vm.isActive = true;
                    vm.isLoading = false;
                    vm.noChat = false;
                    vm.isRequest = false;
                    vm.scrollToBottom();
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        endChat: function(chatCode) {
            if (!confirm('Are you sure to end this conversation?')) {
                return false;
            }

            var host = this.getHost();
            vm.isLoading = true;
            clearInterval(vm.watcher);
            axios.post(host + '/client/chat-panel/close', {chatCode: chatCode })
                .then(function(response) {
                    var responseData = response.data;
                    if (responseData.status == 200) {
                        vm.fetchChat();
                        vm.updateMessage();
                    }

                    vm.isLoading = false;
                    vm.noChat = true;
                    vm.isRequest = false;
                    vm.activeMenu = null;
                })
                .catch(function(error) {
                    vm.isLoading = false;
                    vm.noChat = true;
                    vm.isRequest = false;
                    vm.activeMenu = null;
                    alert('There is something error. Please try again!');
                });
        },
        sendMessage: function() {
            var host = this.getHost();
            vm = this;
            if (!this.isEmptyOrSpaces(vm.message) && vm.message.length > 0) {
                var messageCount = vm.messageList.length;
                if (messageCount > 0) {
                    // console.log(vm.messageList[messageCount - 1]);
                }
                var messageDate = this.formatDateTime(new Date());
                vm.message = vm.garbleToEmoji(vm.message.replace(/&nbsp;/g, ' '));
                vm.messageList.push({
                    agent_id: vm.currentChat.agentId,
                    chat_code: vm.currentChat.chatCode,
                    chat_panel_session_id: vm.currentChat.chatId,
                    client_id: vm.currentChat.clientId,
                    content: vm.message,
                    created_date: messageDate,
                    from: '1',
                    name: vm.currentChat.name,
                    number: vm.currentChat.number,
                    project_id: vm.currentChat.projectId,
                    project_name: vm.currentChat.projectName,
                    wa_media_id: null,
                    file_name: null,
                    download_status: 0
                });

                vm.scrollToBottom();
                // console.log(vm.messageList);
                axios.post(host + '/client/chat-panel/do-send', {
                    chatCode: vm.chatCode,
                    message: vm.message
                })
                    .then(function(response) {
                        if (response.status == 200) {
                            vm.lastMessage = messageDate;
                        }
                    });
                    vm.message = '';
            } else {
                alert('Message not allowed empty!');
                return false;
            }
        },
        updateMessage: function() {
            var host = this.getHost();
            vm = this;
            // console.log(vm.chatCode);
            // console.log(vm.lastMessage);
            axios.post(host + '/client/chat-panel/update-chat', {
                chatCode: vm.chatCode,
                lastMessage: vm.lastMessage
            })
                .then(function(response) {
                    var responseData = response.data;

                    if (responseData.data.length > 0) {
                        var messages = responseData.data;
                        // console.log(vm.lastMessage);
                        // console.log(vm.lastMessage)
                        vm.messageList = messages;
                        // messages.forEach(function(item, index) {
                        //     vm.messageList.push({
                        //         agent_id: vm.currentChat.agentId,
                        //         chat_code: vm.currentChat.chatCode,
                        //         chat_panel_session_id: item.chat_panel_session_id,
                        //         client_id: item.client_id,
                        //         content: item.content,
                        //         created_date: item.created_date,
                        //         from: '0',
                        //         name: item.name,
                        //         number: item.number,
                        //         project_id: item.project_id,
                        //         project_name: item.project_name
                        //     });
                        //     // vm.lastMessage = item.created_date;
                        // });
                        vm.lastMessage = messages[messages.length - 1].created_date;
                        vm.scrollToBottom();
                        // vm.messageList.push(responseData.data);
                        // console.log(messages)
                    }
                    // console.log(vm.lastMessage);
                    // console.log(responseData.data);
                });
        },
        chatListWatcher: function() {
            // console.log('chat list watcher');
            var host = this.getHost();
            vm = this;
            const cancelToken = axios.CancelToken;
            vm.tokenSource = cancelToken.source();
            // clearInterval(vm.watcher);
            axios.get(host + '/client/chat-panel/get-chat', {
                cancelToken: vm.tokenSource.token
            })
                .then(function(response) {
                    const responseData = response.data;
                    if (responseData.status == 200) {
                        vm.chatResults = responseData.data;
                        vm.totalChatRequest = vm.chatResults.filter(function(chat) {
                            return chat.agent_id == null;
                        }).length;
                        vm.totalChatActive = vm.chatResults.filter(function(chat) {
                            return chat.active == 1 && chat.agent_id != null;
                        }).length
                        vm.filterChat();
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        scrollToBottom: function() {
            setTimeout(function() {
                var container = document.querySelector('.chat-area');
                var chatArea = document.getElementById('bottomChat');
                var scrollHeight = chatArea.scrollHeight;
                container.scrollTop = (scrollHeight);
            }, 50);
        },
        inputKeyup: function(e) {
            if (e.key == 'Enter') {
                this.sendMessage();
                e.target.innerHTML = ''
            }
        },
        inputKeyupCaption: function(e) {
            if (e.key == 'Enter') {
                this.sendFile();
                e.target.innerHTML = ''
            }

            if (e.target.innerHTML.length > 1024) {
                var text = e.target.innerHTML = vm.caption.substr(0, 1024);
                e.target.innerHTML = text;
                vm.caption = text;
                e.target.selectionStart = e.target.selectionEnd = e.target.innerHTML.length;
            }
        },
        inputKeydownCaption: function(e) {
            // console.log(e.target.innerHTML.length);
            // if (e.target.innerHTML.length >= 5) {
            //     e.preventDefault();
            //     return false;
            // }
        },
        isEmptyOrSpaces: function(str) {
            return str === null || str.match(/^ *$/) !== null;
        },
        formatDateTime: function(date) {
            return date.getFullYear()+'-'+
                `${date.getMonth() + 1}`.padStart(2, 0)+'-'+
                `${date.getDate()}`.padStart(2, 0)+' '+
                `${date.getHours()}`.padStart(2, 0)+':'+
                `${date.getMinutes()}`.padStart(2, 0)+':'+
                `${date.getSeconds()}`.padStart(2, 0);

        },
        formatTime: function(stringDate) {
            stringDate = stringDate.replace('-', '/').replace('-', '/');
            var date = new Date(stringDate);
            var hour = date.getHours();
            var minute = date.getMinutes();
            var ampm = 'AM';

            if (hour >= 12) {
                ampm = 'PM';
            }

            return `${hour}`.padStart(2, 0) + ':' + `${minute}`.padStart(2, 0) + ' ' + ampm;
        },
        getDate: function(stringDate) {
            var date = new Date(stringDate);
            return date.getDate();
        },
        getWeekday: function(stringDate) {
            const getYear = stringDate.substr(0, 4);
            const getMonth = stringDate.substr(5, 2);
            const getDate = stringDate.substr(8, 2);
            const temp = new Date(stringDate);
            const year = temp.getFullYear();
            const month = temp.getMonth();
            const day = temp.getDate();
            const date = new Date(year, month, day);
            const now = new Date();
            now.setHours(0,0,0,0);

            var weekday = date.toLocaleString('en-US', {
                day: 'numeric',
                month: 'long',
                year: 'numeric'
            });

            if ((now.valueOf() - date.valueOf()) <= 518400000) {
                weekday = date.toLocaleString('en-US', {
                    weekday: 'long'
                });
            }

            if ((now.valueOf() - date.valueOf()) == 86400000) {
                weekday = 'Yesterday';
            }
            
            if (date.valueOf() == now.valueOf()) {
                weekday = 'Today';
            }

            return weekday;
        },
        // checkNewRequest: function() {
        //     const host = this.getHost();
        //     axios.post(host + '/client/chat-panel/check-new-request', {
        //         totalRequest: vm.totalChatRequest
        //     })
        //         .then(function(response) {
        //             const responseData = response.data;
        //             if (responseData.status == 200) {
        //                 responseData.data.forEach(function(item, index) {
        //                     const checkNumber = vm.chatResults.filter(function(chat) {
        //                         return chat.number == item.number;
        //                     }).length;
        //                     if (checkNumber > 0) {
        //                         vm.chatResults.splice(vm.chatResults.findIndex(e => e.number === item.number),1);
        //                     }
        //                     vm.chatResults.push({
        //                         active: item.active,
        //                         agent_id: null,
        //                         chat_code: item.chat_code,
        //                         created_date: item.created_date,
        //                         last_message_date: item.last_message_date,
        //                         number: item.number,
        //                         name: item.name,
        //                         rn: "1"
        //                     });
        //                 })
        //                 vm.totalChatRequest += responseData.data.length;
        //                 vm.filteredChat = vm.sortingChat(vm.chatResults);
        //             }
        //         })
        //         .catch(function(error) {
        //             console.log(error);
        //         });
        // },
        requestChatWatcher: function() {
            vm = this;
            // setInterval(function() {
            //     // console.log('request watcher');
            //     vm.checkNewRequest();
            // }, 3000);
            vm.chatWatcher = setInterval(function() {
                vm.chatListWatcher();
            }, 3000);
        },
        filterActive: function() {
            vm = this;
            if (vm.isPanelHistory) {
                return false;
            }

            vm.isTotalActive = !vm.isTotalActive;
            if (vm.isTotalActive && vm.isTotalRequest) {
                vm.filteredChat = vm.chatResults.filter(function(chat) {
                    return chat.active == 1 || chat.agent_id == null || chat.active == 0;
                });
            } else if (vm.isTotalActive && !vm.isTotalRequest) {
                vm.filteredChat = vm.chatResults.filter(function(chat) {
                    return chat.active == 1 && chat.agent_id != null || chat.active == 0;
                });
            } else if (!vm.isTotalActive && vm.isTotalRequest) {
                vm.filteredChat = vm.chatResults.filter(function(chat) {
                    return chat.active != 1 || chat.agent_id == null || chat.active == 0;
                });
            } else if (!vm.isTotalActive && !vm.isTotalRequest) {
                vm.filteredChat = vm.chatResults.filter(function(chat) {
                    return chat.active != 1 && chat.agent_id != null || chat.active == 0;
                });
            }
        },
        filterRequest: function() {
            vm = this;
            if (vm.isPanelHistory) {
                return false;
            }

            vm.isTotalRequest = !vm.isTotalRequest;
            if (vm.isTotalRequest && vm.isTotalActive) {
                vm.filteredChat = vm.chatResults.filter(function(chat) {
                    return chat.agent_id == null || chat.active == 1 || chat.active == 0;
                });
            } else if (vm.isTotalRequest && !vm.isTotalActive) {
                vm.filteredChat = vm.chatResults.filter(function(chat) {
                    return chat.agent_id == null || chat.active == 0;
                });
            } else if (!vm.isTotalRequest && vm.isTotalActive) {
                vm.filteredChat = vm.chatResults.filter(function(chat) {
                    return chat.agent_id != null && chat.active == 1 || chat.active == 0;
                });
            } else if (!vm.isTotalRequest && !vm.isTotalActive) {
                vm.filteredChat = vm.chatResults.filter(function(chat) {
                    return chat.agent_id != null && chat.active != 1 || chat.active == 0;
                });
            }
        },
        sortingChat: function(messages) {
            return messages.sort(function(a, b) {
                const dateA = new Date(a.created_date);
                const dateB = new Date(b.created_date);

                return a.agent_id - b.agent_id || dateB - dateA;
            });
        },
        chatPanelHistory: function() {
            vm = this;
            vm.isPanelHistory = !vm.isPanelHistory;
            vm.isLoading = true;
            vm.placeholders = true;
            vm.noChat = true;
            vm.isRequest = false;
            vm.activeMenu = null;
            vm.chatCode = null;
            clearInterval(vm.chatWatcher);
            vm.chatWatcher = null;

            if (vm.isPanelHistory) {
                vm.isTotalActive = false;
                vm.isTotalRequest = false;
                const host = vm.getHost();
                vm.tokenSource.cancel('Operation canceled.');
                axios.post(host + '/client/chat-panel/get-chat-history')
                    .then(function(response) {
                        const responseData = response.data;
                        if (responseData.status == 200) {
                            vm.chatResults = responseData.data;
                            vm.filteredChat = vm.chatResults;
                            vm.isLoading = false;
                            vm.placeholders = false;
                        }
                    })
                    .catch(function(error) {
                        console.log(error)
                    })
            } else {
                vm.isTotalActive = true;
                vm.isTotalRequest = true;
                vm.fetchChat();
            }
        },
        buttonScroll: function() {
            vm = this;
            //  = function() {
            //     alert('oke')
            //     // if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            //     //     document.getElementById("myBtn").style.display = "block";
            //     // } else {
            //     //     document.getElementById("myBtn").style.display = "none";
            //     // }
            // }
        },
        expandHistory: function() {
            vm = this;
            vm.isExpanded = !vm.isExpanded;

            if (vm.isExpanded) {

            }
        },
        toggleEmoji: function() {
            vm = this;
            vm.emojiExpanded = !vm.emojiExpanded;
            var container = document.querySelector('.chat-area');
            var chatArea = document.getElementById('bottomChat');
            var scrollHeight = chatArea.scrollHeight;
            if (vm.emojiExpanded) {
                $('.chat-area').animate({scrollTop: (container.scrollTop + 299)}, 300);
                // container.scrollTop = (container.scrollTop + 240);
            } else {
                $('.chat-area').animate({scrollTop: (container.scrollTop - 299)}, 300);
            }
            // setTimeout(function() {
            //     var container = document.querySelector('.chat-area');
            //     var chatArea = document.getElementById('bottomChat');
            //     var scrollHeight = chatArea.scrollHeight;
            //     container.scrollTop = (scrollHeight);
            // }, 200)
        },
        scrollEmoji: function(elKey) {
            vm = this;
            vm.emojiActive = elKey;
            vm.emojiClick = true;
            $('div.active-marker').css('transform', 'translateX(' + (-400 + (elKey * 100)) + '%)');
            $('div.emoji-content').animate({
                scrollTop: $("div.emoji-content-list__header:eq("+elKey+")").position().top,
            }, {
                duration: 300,
                complete: function() {
                    vm.emojiClick = false;
                }
            });

            // if ($('div.emoji-content').scrollTop() > $("div.emoji-content-list__header:eq("+elKey+")").position().top) {
            //     console.log('left');
            // } else {
            //     console.log('right');
            // }
        },
        getEmojiCode: function(emojiCode) {
            vm = this;
            function unescapeUnicode(str) {
                return str.replace(/\\u([a-fA-F0-9]{4})/g, function(g, m1) {
                    return String.fromCharCode(parseInt(m1, 16));
                });
            }
            vm.message += twemoji.parse(unescapeUnicode(decodeURIComponent(emojiCode.replace(/\+/g, ' '))));
            // vm.message = vm.garbleToEmoji(vm.messageContent);
        },
        messageEvent: function(e) {
            vm = this;
            // vm.messageContent = e.target.innerHTML;
            // vm.message = vm.garbleToEmoji(vm.messageContent);
        },
        garbleToEmoji: function(image) {
            return image.replace(/<img\b[^<>]*?src=['"].*?\/([^.\/<>]*)\.[^.\/<>]*['"][^><]*?>/g, function(g, m1) {
                var encodeUrl = escape(twemoji.convert.fromCodePoint(m1));
                return encodeUrl.replace(/%/g, '\\');
            });
        },
        handleFileUpload: function(e) {
            vm = this;
            vm.file = e.target.files[0];
            vm.urlPreview = URL.createObjectURL(vm.file);

            setTimeout(function() {
                $('.send-caption-input').focus();
            }, 400)
            
            const height = $('.chat-panel').height();
            $('.file-preview-body .img-preview').css('height', (height - 160) + 'px');
            $('.file-upload-preview').toggleClass('file-preview');
        },
        cancelUpload: function() {
            vm = this;
            vm.urlPreview = null;
            vm.file = null;
            vm.caption = '';
            $('.file-upload-preview').removeClass('file-preview');
        },
        sendFile: function() {
            vm = this;
            const host = vm.getHost();
            const formData = new FormData();
            formData.append('file', vm.file);
            formData.set('caption', vm.caption);
            formData.set('chatCode', vm.chatCode);
            vm.urlPreview = null;
            vm.file = null;
            vm.caption = '';
            $('.file-upload-preview').removeClass('file-preview');

            axios.post(host + '/client/chat-panel/single-file',
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            ).then(function(response) {
                const responseData = response.data;
                vm.messageList.push({
                    agent_id: vm.currentChat.agentId,
                    chat_code: vm.currentChat.chatCode,
                    chat_panel_session_id: vm.currentChat.chatId,
                    client_id: vm.currentChat.clientId,
                    content: null,
                    created_date: messageDate,
                    from: '1',
                    name: vm.currentChat.name,
                    number: vm.currentChat.number,
                    project_id: vm.currentChat.projectId,
                    project_name: vm.currentChat.projectName,
                    file_name: responseData.data.data.filename,
                    caption: responseData.data.data.caption
                });
                vm.caption = '';
                vm.file = null;
                vm.urlPreview = null;
            })
            .catch(function() {
                console.log('FAILURE!!');
            });
        },
        getImageMessage: function(filename) {
            return '/uploads/chat-panel/temp/' + filename;
        },
    },
    created: function() {
        vm = this;
        vm.fetchChat();
    },
    mounted: function() {
        vm = this;
        var topMenu = $("div.emoji-header"),
        topMenuHeight = topMenu.outerHeight() + 15,
        menuItems = topMenu.find('div.emoji-header__item'),
        scrollItems = $('div.emoji-content-list').find('div.emoji-content-list__header');

        $('div.emoji-content').scroll(function(){
            var fromTop = $(this).scrollTop();
            var cur = scrollItems.map(function(){
                if ($(this).position().top <= fromTop)
                return this;
            });
            cur = cur[cur.length - 1];
            var index = $('div.emoji-content-list>div.emoji-content-list__header').index($(cur));
            if (vm.emojiClick == false) {
                vm.emojiActive = index;
                $('div.active-marker').css('transform', 'translateX(' + (-400 + (index * 100)) + '%)');
            }
        });
    }
});